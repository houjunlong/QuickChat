﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuickChat
{
    public class ChatMessage
    {
        public String content;
        public String time;
    }

    public class ChatRecord
    {
        public string username;
        public ChatMessage messsage;

        public ChatRecord()
        {
            messsage = new ChatMessage();
        }
    }

    public class ChatRecords
    {
        public string username;
        public string friendname;
        public List<ChatRecord> records;

        public ChatRecords()
        {
            records = new List<ChatRecord>();
        }
    }

    public class User
    {
        public string IP{get;set;}
        public string username{get;set;}
    }

    public class LocalUser
    {
        public string username;
        public string password;
    }
}
