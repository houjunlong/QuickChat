﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickChat
{
    public partial class RegistetForm : Form
    {
        private string username;
        private string password;

        public RegistetForm()
        {
            InitializeComponent();
        }

        private void btnRegister_Click(object sender, EventArgs e)
        {
            username = tbName.Text;
            password = tbPassword.Text;

            if (username.Length != 0 && password.Length != 0)
            {
                if (FileManagement.SaveUserInfo(username, password) == false) 
                {
                    MessageBox.Show("用户名已存在！请重新输入用户名");
                }
                else
                {
                    MessageBox.Show("注册成功！");
                }
                this.Close();
            }
            else
                MessageBox.Show("用户名和密码均不能为空");
        }
    }
}
