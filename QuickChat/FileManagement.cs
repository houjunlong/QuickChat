﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace QuickChat
{
    public static class FileManagement
    {
        public static bool SaveChatRecords(ChatRecords records)
        {
            XmlDocument xmlfile = new XmlDocument();
            XmlNode userdata;
            XmlNode user;
            XmlNode chatrecords;
            XmlNode friend;
            string filepath = @"..\..\Data\ChatRecords.xml";

            try
            {
                xmlfile.Load(filepath);

                userdata = xmlfile.SelectSingleNode("userdata");

                if (userdata.SelectSingleNode(records.username) == null)
                {
                    user = xmlfile.CreateElement(records.username);
                    userdata.AppendChild(user);
                }
                else
                {
                    user = userdata.SelectSingleNode(records.username);
                }

                if (user.SelectSingleNode("chatRecords") == null)
                {
                    chatrecords = xmlfile.CreateElement("chatRecords");
                    user.AppendChild(chatrecords);
                }
                else
                {
                    chatrecords = user.SelectSingleNode("chatRecords");
                }

                if (chatrecords.SelectSingleNode(records.friendname) == null)
                {
                    friend = xmlfile.CreateElement(records.friendname);
                    chatrecords.AppendChild(friend);
                }
                else
                {
                    friend = chatrecords.SelectSingleNode(records.friendname);
                }

                foreach (ChatRecord record in records.records)
                {
                    XmlNode Record = xmlfile.CreateElement("Record");

                    XmlNode data;
                    data = xmlfile.CreateElement("User");
                    data.InnerText = record.username;
                    Record.AppendChild(data);

                    data = xmlfile.CreateElement("time");
                    data.InnerText = record.messsage.time;
                    Record.AppendChild(data);

                    data = xmlfile.CreateElement("content");
                    data.InnerText = record.messsage.content;
                    Record.AppendChild(data);

                    friend.AppendChild(Record);
                }
                xmlfile.Save(filepath);
                return true;
            }
            catch
            {
                return false;
            }
        }   
        
        public static bool GetChatRecords(ChatRecords records, string username,string friendname)
        {
            XmlDocument xmlfile = new XmlDocument();
            string filepath = @"..\..\Data\ChatRecords.xml";

            try
            {
                xmlfile.Load(filepath);

                XmlNode friend = xmlfile.SelectSingleNode("/userdata/" + username + "/chatRecords/" + friendname);

                foreach (XmlNode Record in friend.ChildNodes)
                {
                    ChatRecord temp = new ChatRecord();
                    temp.username = Record.ChildNodes[0].InnerText;
                    temp.messsage.time = Record.ChildNodes[1].InnerText;
                    temp.messsage.content = Record.ChildNodes[2].InnerText;
                    records.records.Add(temp);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool SaveUserInfo(string username,string password)
        {
            XmlDocument xmlfile = new XmlDocument();
            XmlNode userinfo;
            XmlNode user;
            XmlNode pass;
            string filepath = @"..\..\Data\UserInfo.xml";

            try
            {
                xmlfile.Load(filepath);
                userinfo = xmlfile.SelectSingleNode("userinfo");

                if (userinfo.SelectSingleNode(username) == null)
                {
                    user = xmlfile.CreateElement(username);
                    pass = xmlfile.CreateElement("password");
                    pass.InnerText = password;
                    user.AppendChild(pass);
                    userinfo.AppendChild(user);
                    xmlfile.Save(filepath);
                }
                else
                {
                    return false;
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool GetUserInfo(List<LocalUser> users)
        {
            XmlDocument xmlfile = new XmlDocument();
            XmlNodeList userinfos;
            string filepath = @"..\..\Data\UserInfo.xml";

            try
            {
                xmlfile.Load(filepath);

                userinfos = xmlfile.SelectSingleNode("userinfo").ChildNodes;
                foreach(XmlNode node in userinfos)
                {
                    LocalUser lu = new LocalUser();
                    lu.username = node.LocalName;
                    lu.password = node.FirstChild.InnerText;

                    users.Add(lu);
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
