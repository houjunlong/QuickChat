﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickChat
{
    public partial class ChatRecordFrom : Form
    {
        public ChatRecordFrom(string username,string friendname)
        {
            InitializeComponent();
            ChatRecords cr = new ChatRecords();
            FileManagement.GetChatRecords(cr,username,friendname);

            foreach (ChatRecord c in cr.records)
            {
                if (c.username.Equals(username))
                {
                    rtbMessages.SelectionColor = Color.Green;
                    rtbMessages.AppendText(c.messsage.time + "  " + c.username + "  " + c.messsage.content);
                }
                else
                {
                    rtbMessages.SelectionColor = Color.Black;
                    rtbMessages.AppendText(c.messsage.time + "  " + c.username + "  " + c.messsage.content);
                }
            }
        }
    }
}
