﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace QuickChat
{
    public partial class UserForm : Form
    {
        private string username;
        private string password;
        private List<LocalUser> users;

        public UserForm()
        {
            this.StartPosition = FormStartPosition.CenterScreen;
            InitializeComponent();
            RefreshUsers();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            username = cmbUserName.Text.ToString();
            password = tbPassword.Text;

            foreach (LocalUser lu in users)
            {
                if (lu.username.Equals(username) && lu.password.Equals(password))
                {
                    ChatForm chatform = new ChatForm(username);

                    chatform.StartPosition = FormStartPosition.CenterScreen;
                    chatform.ShowDialog();
                    Application.Exit();
                    return;
                }
            }
            MessageBox.Show("用户名或密码不正确！\n请检查您的输入或者注册新账户！");
            tbPassword.Clear();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void lblRegister_Click(object sender, EventArgs e)
        {
            RegistetForm rform = new RegistetForm();

            rform.StartPosition = FormStartPosition.CenterScreen;
            rform.ShowDialog();
            RefreshUsers();
        }

        private void RefreshUsers()
        {
            users = new List<LocalUser>();
            FileManagement.GetUserInfo(users);

            cmbUserName.Items.Clear();

            foreach (LocalUser lu in users)
            {
                cmbUserName.Items.Add(lu.username);
            }
        }
    }
}
