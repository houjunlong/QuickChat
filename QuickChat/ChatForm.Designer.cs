﻿namespace QuickChat
{
    partial class ChatForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChatForm));
            this.btnSend = new System.Windows.Forms.Button();
            this.rtbMessages = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnSendFile = new System.Windows.Forms.Button();
            this.btnChatRecord = new System.Windows.Forms.Button();
            this.lbUsers = new System.Windows.Forms.ListBox();
            this.btnQuitChat = new System.Windows.Forms.Button();
            this.btnExit = new System.Windows.Forms.Button();
            this.rtbInput = new System.Windows.Forms.RichTextBox();
            this.lblChatTo = new System.Windows.Forms.Label();
            this.lblFile = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnSend
            // 
            this.btnSend.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.btnSend, "btnSend");
            this.btnSend.Name = "btnSend";
            this.btnSend.UseVisualStyleBackColor = false;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // rtbMessages
            // 
            resources.ApplyResources(this.rtbMessages, "rtbMessages");
            this.rtbMessages.Name = "rtbMessages";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Name = "label2";
            // 
            // btnSendFile
            // 
            this.btnSendFile.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.btnSendFile, "btnSendFile");
            this.btnSendFile.Name = "btnSendFile";
            this.btnSendFile.UseVisualStyleBackColor = false;
            this.btnSendFile.Click += new System.EventHandler(this.btnSendFile_Click);
            // 
            // btnChatRecord
            // 
            this.btnChatRecord.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.btnChatRecord, "btnChatRecord");
            this.btnChatRecord.Name = "btnChatRecord";
            this.btnChatRecord.UseVisualStyleBackColor = false;
            this.btnChatRecord.Click += new System.EventHandler(this.btnChatRecord_Click);
            // 
            // lbUsers
            // 
            this.lbUsers.FormattingEnabled = true;
            resources.ApplyResources(this.lbUsers, "lbUsers");
            this.lbUsers.Name = "lbUsers";
            this.lbUsers.DoubleClick += new System.EventHandler(this.lbUsers_DoubleClick);
            // 
            // btnQuitChat
            // 
            this.btnQuitChat.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.btnQuitChat, "btnQuitChat");
            this.btnQuitChat.Name = "btnQuitChat";
            this.btnQuitChat.UseVisualStyleBackColor = false;
            this.btnQuitChat.Click += new System.EventHandler(this.btnQuitChat_Click);
            // 
            // btnExit
            // 
            this.btnExit.BackColor = System.Drawing.SystemColors.Info;
            resources.ApplyResources(this.btnExit, "btnExit");
            this.btnExit.Name = "btnExit";
            this.btnExit.UseVisualStyleBackColor = false;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // rtbInput
            // 
            resources.ApplyResources(this.rtbInput, "rtbInput");
            this.rtbInput.Name = "rtbInput";
            // 
            // lblChatTo
            // 
            this.lblChatTo.BackColor = System.Drawing.Color.MintCream;
            resources.ApplyResources(this.lblChatTo, "lblChatTo");
            this.lblChatTo.Name = "lblChatTo";
            // 
            // lblFile
            // 
            resources.ApplyResources(this.lblFile, "lblFile");
            this.lblFile.Name = "lblFile";
            // 
            // ChatForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::QuickChat.Properties.Resources.chatbackgroud;
            this.Controls.Add(this.lblFile);
            this.Controls.Add(this.lblChatTo);
            this.Controls.Add(this.rtbInput);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnQuitChat);
            this.Controls.Add(this.lbUsers);
            this.Controls.Add(this.btnChatRecord);
            this.Controls.Add(this.btnSendFile);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.rtbMessages);
            this.Controls.Add(this.btnSend);
            this.Name = "ChatForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.RichTextBox rtbMessages;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSendFile;
        private System.Windows.Forms.Button btnChatRecord;
        private System.Windows.Forms.ListBox lbUsers;
        private System.Windows.Forms.Button btnQuitChat;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.RichTextBox rtbInput;
        private System.Windows.Forms.Label lblChatTo;
        private System.Windows.Forms.Label lblFile;
    }
}

